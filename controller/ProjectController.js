const Project = require('../models/admin/Project')
const helper = require('../utils/Helper')


exports.renderProjects = async (req, res) => {
    if (req.session.isLoggedIn) {
        const name = req.session.name
        res.render("create-project", { name: name })

    } else {
        res.redirect('/admin/login')

    }
}

exports.addProject = async (req, res) => {

    var title = req.body.project_title
    var projectImage = req.file
    var projectDetails = req.body.project_info
    if (title === "") {
        req.flash('error', 'Please add project title')
        res.redirect('/admin/projects/add')

    } else if (projectDetails === "") {
        req.flash('error', 'Please add project details')
        res.redirect('/admin/projects/add')

    } else if (projectImage === undefined) {
        req.flash('error', 'Please upload project image')
        res.redirect('/admin/projects/add')
    } else {

        await Project.create({
            project_title: title,
            project_image: helper.createFileUrl(projectImage),
            project_description: projectDetails

        }).then(data => {
            req.flash('success', 'Created Successfully')
            res.redirect('/admin/projects/add')

        }).catch(error => {
            req.flash('error', 'Something went wrong')
            console.log(error)
        })
    }



}

exports.getAllProjects = async (req, res) => {
    if (req.session.isLoggedIn) {
        await Project.findAll().then(data => {

            res.render("project-all", { projects: data, name: req.session.name })

        }).catch(error => {
            console.log(error);
        })

    } else {
        res.redirect('/admin/login')

    }


}


exports.getProjectDetails = async (req, res) => {
    if (req.session.isLoggedIn) {
        var projectID = req.params.id
        await Project.findOne({ where: { project_id: projectID } }).then(data => {
            res.render("single-project-admin", { project: data, name: req.session.name })
        }).catch(err => {
            console.log(err)
        })
    } else {


        res.redirect('/admin/login')
    }

}

