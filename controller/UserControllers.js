

const Quotes = require('../models/users/Quotes')
const Project = require('../models/admin/Project')
const News = require('../models/admin/News')

exports.getHome = async (req, res) => {
    var recentNews;
    await News.findAll({ order: [['news_id', 'DESC']], limit: 3 }).then(news => { recentNews = news }).catch(error => {
        console.log(error)
    })

    await Project.findAll({ order: [['project_id', 'DESC']], limit: 3 }).then(data => {

        res.render("index", { projects: data, headlines: recentNews })
    }).catch(error => {
        console.log(error)
    })

}


exports.getAboutUs = async (req, res) => {
    var recentNews;
    await News.findAll({ order: [['news_id', 'DESC']], limit: 3 }).then(news => { recentNews = news }).catch(error => {
        console.log(error)
    })

    res.render("about", { headlines: recentNews })
}



exports.getServices = async (req, res) => {
    var recentNews;
    await News.findAll({ order: [['news_id', 'DESC']], limit: 3 }).then(news => { recentNews = news }).catch(error => {
        console.log(error)
    })

    res.render("services", { headlines: recentNews })
}

exports.getProjects = async (req, res) => {
    var recentNews;
    await News.findAll({ order: [['news_id', 'DESC']], limit: 3 }).then(news => { recentNews = news }).catch(error => {
        console.log(error)
    })
    await Project.findAll().then(data => {

        res.render("projects", { projects: data, headlines: recentNews })
    }).catch(error => {
        console.log(error)
    })

}

exports.getQuote = async (req, res) => {
    var recentNews;
    await News.findAll({ order: [['news_id', 'DESC']], limit: 3 }).then(news => { recentNews = news }).catch(error => {
        console.log(error)
    })

    res.render("quote", { headlines: recentNews })
}

exports.addQuotes = async (req, res) => {
    await Quotes.create({
        first_name: req.body.first_name,
        last_name: req.body.last_name,
        email: req.body.email,
        phone: req.body.phone,
        about_company: req.body.your_company,
        about_project: req.body.your_project

    }).then(onSuccess => {
        req.flash('success','Successfully Sent.We will contact you soon.')
        res.redirect('/quote')

    }).catch(error => {
        console.log(error)
        req.flash('error','Something went wrong')
    })
}


exports.renderSingleProject = async (req, res) => {
    var recentNews;
    await News.findAll({ order: [['news_id', 'DESC']], limit: 3 }).then(news => { recentNews = news }).catch(error => {
        console.log(error)
    })


    const projectID = req.params.id
    await Project.findOne({ where: { 'project_id': projectID } }).then(data => {

        res.render("single-project", { project: data, headlines: recentNews })
    }).catch(error => {

        console.log(error)
    })
}